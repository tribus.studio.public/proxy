NETWORK_PROXY_CHECK := $(shell docker network ls | grep -w proxy_default | awk '{print $$1}')
COLUMN_NAMES := \
	{{.Names}} \
	\t{{.ID}} \
	\t{{.State}}

include .env

#  If you want to find out the value of a makefile variable: make print-VARIABLE
print-% : ; $(info $* is a $(flavor $*) variable set to [$($*)]) @true

default: help

## help	:	Print commands help.
.PHONY: help
help : Makefile
	@sed -n 's/^##//p' $<

## list	: 	List all commands. You can type 'make{tab}' and it will list all available files and commands.
.PHONY: list
list:
#	replaced $(lastword $(MAKEFILE_LIST) since this includes the .env file.
	@$(MAKE) -pRrq -f Makefile : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]:]|Makefile' -e '^$@$$'

.PHONY: network_create
network_create:
ifeq ($(NETWORK_PROXY_CHECK),)
	@docker network create proxy_default 
endif	

## up	:	Start up containers.
.PHONY: up
up: network_create
	@echo "Starting up containers for $(PROJECT_NAME)..."
	$(eval ARGS := $(filter-out $@,$(MAKECMDGOALS)))
	@if [ -z '$(ARGS)' ]; then \
		PROFILES=''; \
	else \
		for profile in $(ARGS); do \
			if [ $$profile = 'port' ]; then \
				PROFILES=$$PROFILES'--profile port '; \
			fi; \
			if [ $$profile = 'who' ]; then \
				PROFILES=$$PROFILES'--profile who '; \
			fi; \
		done; \
	fi; \
	echo $$PROFILES; \
	docker compose $$PROFILES up -d

## down	:	Stop containers.
.PHONY: down
down:
	@echo "Shutting down containers for $(PROJECT_NAME)..."
	$(eval ARGS := $(filter-out $@,$(MAKECMDGOALS)))
	@if [ -z '$(ARGS)' ]; then \
		PROFILES=''; \
	else \
		for profile in $(ARGS); do \
			if [ $$profile = 'port' ]; then \
				PROFILES=$$PROFILES'--profile port '; \
			fi; \
			if [ $$profile = 'who' ]; then \
				PROFILES=$$PROFILES'--profile who '; \
			fi; \
		done; \
	fi; \
	docker compose $$PROFILES down

## start	:	Start containers without updating.
.PHONY: start
start:
	@echo "Starting containers for $(PROJECT_NAME) from where you left off..."
	@docker-compose start

## restart:	Restart container without stopping
.PHONY: restart
restart:
	@docker-compose restart

## stop	:	Stop containers.
.PHONY: stop
stop:
	@echo "Stopping containers for $(PROJECT_NAME)..."
	$(eval ARGS := $(filter-out $@,$(MAKECMDGOALS)))
	@if [ -z '$(ARGS)' ]; then \
		PROFILES=''; \
	else \
		for profile in $(ARGS); do \
			if [ $$profile = 'port' ]; then \
				PROFILES=$$PROFILES'--profile port '; \
			fi; \
			if [ $$profile = 'who' ]; then \
				PROFILES=$$PROFILES'--profile who '; \
			fi; \
		done; \
	fi; \
	docker compose $$PROFILES stop

## ps			: List running containers by Name, ID, Status and Image name.
##			  You can add some arguments to this command:
##			  - make ps created
##			  - make ps ports image labels
.PHONY: ps
ps:
	$(eval ARGS := $(filter-out $@,$(MAKECMDGOALS)))
	$(foreach word,$(COLUMN_NAMES),$(eval COLUMNS := $(COLUMNS)$(word)))
	@if [ -z '$(ARGS)' ]; then \
		COLUMNS='$(COLUMNS)'; \
	else \
		COLUMNS='$(COLUMNS)'; \
		for cols in $(ARGS); do \
			if [ $$cols = 'created' ]; then \
				COLUMNS=$$COLUMNS'\t{{.CreatedAt}}'; \
			fi; \
			if [ $$cols = 'ports' ]; then \
				COLUMNS=$$COLUMNS'\t{{.Ports}}'; \
			fi; \
			if [ $$cols = 'image' ]; then \
				COLUMNS=$$COLUMNS'\t{{.Image}}'; \
			fi; \
			if [ $$cols = 'size' ]; then \
				COLUMNS=$$COLUMNS'\t{{.Size}}'; \
			fi; \
			if [ $$cols = 'command' ]; then \
				COLUMNS=$$COLUMNS'\t{{.Command}}'; \
			fi; \
			if [ $$cols = 'status' ]; then \
				COLUMNS=$$COLUMNS'\t{{.Status}}'; \
			fi; \
		done; \
	fi; \
	docker ps --filter name='$(PROJECT_NAME)' --format "table $$COLUMNS"

## logs	:	View containers logs.
##		You can optinally pass an argument with the service name to limit logs
##		logs php	: View `php` container logs.
##		logs nginx php	: View `nginx` and `php` containers logs.
.PHONY: logs
logs:
	@docker-compose logs -f $(filter-out $@,$(MAKECMDGOALS))

# https://stackoverflow.com/a/6273809/1826109
%:
	@:
